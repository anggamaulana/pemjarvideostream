package net.majorkernelpanic.spydroid.ui;



import net.majorkernelpanic.spydroid.R;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class ChanelActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.channel);
		VideoView video = (VideoView)findViewById(R.id.videoView1);
		String uri = getIntent().getExtras().getString("uri");
	       
        video.setVideoURI( Uri.parse(uri) );
        video.setMediaController( new MediaController(getApplicationContext()));
        video.requestFocus();
        video.start();
		
		
	}

}
