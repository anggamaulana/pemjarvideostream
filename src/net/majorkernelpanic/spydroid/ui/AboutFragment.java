/*
 * Copyright (C) 2011-2013 GUIGUI Simon, fyhertz@gmail.com
 * 
 * This file is part of Spydroid (http://code.google.com/p/spydroid-ipcamera/)
 * 
 * Spydroid is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package net.majorkernelpanic.spydroid.ui;

import net.majorkernelpanic.spydroid.R;
import net.majorkernelpanic.spydroid.SpydroidApplication;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class AboutFragment extends Fragment {

	private Button mButtonVisit;
	private Button mButtonRate;
	private Button mButtonLike;
	private Button mRtsp;
	private VideoView video;
	private TextView alamatrtsp;
	private MediaController mediac;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.about,container,false);
		
		mRtsp = (Button)rootView.findViewById(R.id.btnrtsp);
		video = (VideoView)rootView.findViewById(R.id.videoView1);
		alamatrtsp = (TextView)rootView.findViewById(R.id.alamatrtsp);
//		mediac = (MediaController)rootView.findViewById(R.id.mediaController1);
		
		mRtsp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 String uri = alamatrtsp.getText().toString();
				 Bundle bn = new Bundle();
				 bn.putString("uri",uri);
				 Intent i = new Intent(getActivity(),ChanelActivity.class);
				 i.putExtras(bn);
				 startActivity(i);
			       
//			        video.setVideoURI( Uri.parse(uri) );
//			        video.setMediaController( new MediaController(getActivity()));
//			        video.requestFocus();
//			        video.start();
				
			}
		});

//		mButtonVisit = (Button)rootView.findViewById(R.id.visit);
//		mButtonRate = (Button)rootView.findViewById(R.id.rate);
//		mButtonLike = (Button)rootView.findViewById(R.id.like);
//
//		mButtonVisit.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://code.google.com/p/spydroid-ipcamera/"));
//				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//				startActivity(intent);
//			}
//		});
//
//		mButtonRate.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				String appPackageName=SpydroidApplication.getInstance().getApplicationContext().getPackageName();
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appPackageName));
//				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//				startActivity(intent);
//			}
//		});
//
//		mButtonLike.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/spydroidipcamera"));
//				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//				startActivity(intent);
//			}
//		}); 

		return rootView ;
	}

}
